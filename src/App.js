import React from 'react';
import './App.css';
import Context from './global/Context';
import Navbar from './components/navbar/Navbar';
import Modal from './components/navbar/Modal';
import Stories from './components/stories/Stories';
import Create from './components/create/Create';
import Posts from './components/posts/Posts';
import Sidebar from './components/sidebar/Sidebar';


function App() {
  return (
    <div>
      <Context>
        <Navbar />
        <div className="container">
          <Stories />
          <Create />
          <Posts />
          <Sidebar />
        </div>
        <Modal />
      </Context>
    </div>
  );
}

export default App;
