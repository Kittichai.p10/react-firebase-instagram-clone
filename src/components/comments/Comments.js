import React, { useState, useEffect, useContext } from "react";
import { ContextProvider } from "../../global/Context";
import { db } from "../../config";
import "./css/Comments.css";

export default function Comments(props) {
  const { loader, user, publishComment } = useContext(ContextProvider);
  const [state, setState] = useState("");
  const [comments, setComments] = useState([]);

  console.log('comments::: ', comments)

  const postComment = (e) => {
    e.preventDefault();
    publishComment({
      id: props.id,
      comment: state,
    });
    setState("");
  };

  useEffect(() => {
    db.collection("posts")
      .doc(props.id)
      .collection("comments")
      .orderBy("currentTime", "desc")
      .onSnapshot((snp) => {
        setComments(snp.docs.map(doc => doc.data()));
      });
  }, []);

  return (
    <div className="comments">
        {comments.map(comment => (
            <div className="comments-container" key={comment.id}>
                <div className="comments-container-name">
                    {comment.username}
                </div>
                <div className="comments-container-msg">
                    {comment.comment}
                </div>
            </div>
        ))}
        <div className="comments-section">
            {!loader && user ? (
                <form onSubmit={postComment}>
                    <input
                        type="text"
                        className="comment-input"
                        placeholder="Add a comment..."
                        onChange={(e) => setState(e.target.value)}
                        value={state}
                        required
                    />
                </form>
            ) : (
              ""
            )}
        </div>
    </div>
  );
}
