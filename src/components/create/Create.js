import React, { useState, useContext } from "react";
import "./css/Create.css";
import { FaCamera } from "react-icons/fa";
import { ContextProvider } from "../../global/Context";

export default function Create() {
  const { create, loader, user } = useContext(ContextProvider);
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");

  const handleImage = (e) => {
    setImage(e.target.files[0]);
  };

  const createPost = (e) => {
    e.preventDefault();
    create({ title, image });
    setTitle("");
    setImage("");
  };

  return (
    <div>
      {!loader && user ? (
        <div>
          {!loader && user ? (
            <div className="create">
              <form onSubmit={createPost}>
                <div className="create-input">
                  <input
                    type="text"
                    className="create-inputt"
                    placeholder="What are in your mind..."
                    onChange={(e) => setTitle(e.target.value)}
                    value={title}
                    required
                  />
                </div>
                <div className="create-second">
                  <div className="create-second-a">
                    <label htmlFor="file">
                      <FaCamera className="camera" />
                    </label>
                    <input
                      type="file"
                      id="file"
                      className="file"
                      onChange={handleImage}
                      required
                    />
                  </div>
                  <div className="create-second-b">
                    <input type="submit" value="Create" className="btn-sweet" />
                  </div>
                </div>
              </form>
            </div>
          ) : null}
        </div>
      ) : null}
    </div>
  );
}
