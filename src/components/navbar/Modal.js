import React, { useState, useContext } from "react";
import "./css/Modal.css";
import { ContextProvider } from "../../global/Context";

export default function Modal() {
  const { modal, closeModal, register, login } = useContext(ContextProvider);
  const [state, setState] = useState({
    register: true,
    login: false,
  });

  const [inputs, setInputs] = useState({
      username: "",
      email: "",
      password: ""
  });

  const handleInput = (e) => {
      setInputs({
          ...inputs,
          [e.target.name]: e.target.value
      })
  }

  const formsToggle = () => {
    setState({
      ...state,
      register: !state.register,
      login: !state.login,
    });
  };

  const closeForm = (e) => {
    const className = e.target.getAttribute('class');
    if (className === "modal") {
        closeModal();
    }
  }

  const registerUser = (e) => {
    e.preventDefault();
    console.log(inputs);
    register(inputs);
    setInputs({
        username: "", email: "", password: ""
    });
  }

  const userLogin = (e) => {
    e.preventDefault();
    login(inputs);
  }

  return (
    <div>
      {modal ? (
        <div className="modal" onClick={closeForm}>
          <div className="modal-container">
            <div className="modal-modal-form">
              {state.register ? (
                <form onSubmit={registerUser}>
                  <div className="modal-group">
                    <img
                      className="modal-modal-form-logo"
                      src="/images/instagram-logo-word.png"
                      alt=""
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="text"
                      name="username"
                      className="modal_input"
                      placeholder="Username..."
                      onChange={handleInput}
                      value={inputs.username}
                      required
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="email"
                      name="email"
                      className="modal_input"
                      placeholder="Email..."
                      onChange={handleInput}
                      value={inputs.email}
                      required
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="password"
                      name="password"
                      className="modal_input"
                      placeholder="Create password..."
                      onChange={handleInput}
                      value={inputs.password}
                      required
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="submit"
                      value="Register"
                      className="btn btn-smart"
                    />
                  </div>
                  <div className="modal-group">
                    <span onClick={formsToggle}>Already have an account ?{" "}</span>
                  </div>
                </form>
              ) : (
                <form onSubmit={userLogin}>
                  <div className="modal-group">
                    <img
                      className="modal-modal-form-logo"
                      src="/images/instagram-logo-word.png"
                      alt=""
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="email"
                      name="email"
                      className="modal_input"
                      placeholder="Email..."
                      onChange={handleInput}
                      value={inputs.email}
                      required
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="password"
                      name="password"
                      className="modal_input"
                      placeholder="Create password..."
                      onChange={handleInput}
                      value={inputs.password}
                      required
                    />
                  </div>
                  <div className="modal-group">
                    <input
                      type="submit"
                      value="Login"
                      className="btn btn-smart"
                    />
                  </div>
                  <div className="modal-group">
                    <span onClick={formsToggle}>Create a new account ?</span>
                  </div>
                </form>
              )}
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}
