import React, { useContext } from "react";
import "./css/Navbar.css";
import {
  FaSearch,
  FaTelegramPlane,
  FaRegCompass,
  FaRegHeart,
} from "react-icons/fa";
import { MdHome } from "react-icons/md";
import { ContextProvider } from "../../global/Context";

export default function Navbar() {
  const { modal, opanModal, user, loader, logout } = useContext(ContextProvider);

  const opanForms = () => {
    opanModal();
  };
  const userLogout = () => {
    logout();
  };

  const checkUser = () => {
    return !loader ? !loader && user ? (
        <li>
          {user.displayName}/ <span onClick={userLogout}>Logout</span>{" "}
        </li>
      ) : (
        <li onClick={opanForms}>Register/Loing</li>
      ) : '...'
  };

  return (
    <div className="navbar">
      <div className="navbar-first">
        <div className="navbar-first-logo">
          <img
            className="navbar-first-logo-img"
            src="/images/instagram-logo-word.png"
            alt=""
          />
        </div>
      </div>
      <div className="navbar-middle">
        <div className="navbar-middle-search">
          <input
            type="text"
            className="navbar-input-search"
            placeholder="Search"
          ></input>
          <FaSearch className="searchIcon" />
        </div>
      </div>
      <div className="navbar-last">
        <li>
          <MdHome className="navbar-icons" />
        </li>
        <li>
          <FaTelegramPlane className="navbar-icons" />
        </li>
        <li>
          <FaRegCompass className="navbar-icons" />
        </li>
        <li>
          <FaRegHeart className="navbar-icons" />
        </li>
        {checkUser()}
      </div>
    </div>
  );
}
