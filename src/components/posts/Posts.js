import React, { useContext } from "react";
import { ContextProvider } from "../../global/Context";
import Comment from "../comments/Comments";
import './css/Posts.css';

export default function Posts() {
  const { posts } = useContext(ContextProvider);

  console.log('posts::: ', posts);

    return (
        <div>
            {posts.map(post => (
                <div className="posts" key={post.id}>
                    <div className="posts-header">
                        <div className="posts-header-avatar">{post.username[0]}</div>
                        <div className="posts-header-name">{post.username}</div>
                    </div>
                    <div className="posts-img">
                        <img src={post.image} alt={post.image} />
                    </div>
                    <Comment id={post.id} />
                </div>
            ))}
        </div>
    )
}
