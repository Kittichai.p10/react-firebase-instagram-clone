import React, { useState, useContext } from "react";
import { ContextProvider } from "../../global/Context";
import "./css/Sidebar.css";

export default function Sidebar() {
  const { loader, user } = useContext(ContextProvider);
  const username = !loader && user && user.displayName ? user.displayName : '';
  const [state] = useState([
    {id: 1, image: '/images/1.png', name: "Kittichai 1"},
    {id: 2, image: '/images/BTC.svg.png', name: "Kittichai 2"},
    {id: 3, image: '/images/dog.jpg', name: "Kittichai 3"},
    {id: 4, image: '/images/dogecoin.png', name: "Kittichai 4"},
  ]);

  return (
    <div className="sidebar">
      {!loader && user ? (
        <div className="sidebar-user">
          <div className="sidebar-user-avatar">{username[0]}</div>
          <div className="sidebar-user-name">{username}</div>
        </div>
      ) : null}
      <div className="sidebar-list">
        <h3>Suggestions for you</h3>
        {state.map(user => (
            <div className="sidebar-list-user" key={user.id}>
                <div className="sidebar-list-a">
                    <div className="sidebar-list-a-img">
                        <img src={user.image} alt={user.image} />
                    </div>
                    <div className="sidebar-list-a-name">{user.name}</div>
                </div>
                <div className="sidebar-list-b">
                    <a href="">Follow</a>
                </div>
            </div>
        ))}
        {/* <div className="sidebar-list-user">

            <div className="sidebar-list-a">A</div>
            <div className="sidebar-list-b">B</div>


            <div>
                {state.map(user => (
                    <div className="sidebar-user-a" key={user.id}>
                        <div className="sidebar-user-img">
                            <img src={user.image} alt={user.image} />
                        </div>
                        <div className="sidebar-list-a-name">{user.name}</div>
                    </div>
                ))}
            </div>
            <div className="sidebar-list-b">
                    Hello
            </div>
        </div> */}
      </div>
    </div>
  );
}
