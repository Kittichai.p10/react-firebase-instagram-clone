import React, { useState } from 'react';
import './css/Stories.css';

export default function Stories() {

    const [state, setState] = useState([
        {id: 1, image: '/images/1.png', name: "Kittichai 1"},
        {id: 2, image: '/images/BTC.svg.png', name: "Kittichai 2"},
        {id: 3, image: '/images/dog.jpg', name: "Kittichai 3"},
        {id: 4, image: '/images/dogecoin.png', name: "Kittichai 4"},
        {id: 5, image: '/images/kittichai.jpg', name: "Kittichai 5"},
        {id: 6, image: '/images/logo.png', name: "Kittichai 6"},
        {id: 7, image: '/images/kittichai.jpg', name: "Kittichai 7"},
        {id: 8, image: '/images/logo.png', name: "Kittichai 8"},
        {id: 9, image: '/images/kittichai.jpg', name: "Kittichai 9"},
    ]);

    return (
        <div className="stories">

            {state.map(user => (
                <div className="stories-info" key={user.id}>
                    <div className="stories-img">
                        <span>
                            <img src={user.image} alt="user" />
                        </span>
                    </div>
                    <div className="stories-name">{user.name}</div>
                </div>
            ))}
        </div>
    )
}
