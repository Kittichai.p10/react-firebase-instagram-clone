// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyBrKZt0X19QH7UBozD1ksx-pBOxXA-sZ-Y",
    authDomain: "react-instagram-cb084.firebaseapp.com",
    projectId: "react-instagram-cb084",
    storageBucket: "react-instagram-cb084.appspot.com",
    messagingSenderId: "1097980363898",
    appId: "1:1097980363898:web:54e6dddc6e57c0c8d9a7c3",
    measurementId: "G-ZR5LZE809Y"
};

firebase.initializeApp(firebaseConfig);
 const db = firebase.firestore();
 const auth = firebase.auth();
 const storage = firebase.storage();

 export { db, auth, storage }
