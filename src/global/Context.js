import React, { useState, useEffect, createContext } from "react";
import { auth, db, storage } from "../config";
import firebase from "firebase";
export const ContextProvider = createContext();

const Context = (props) => {
  const [modal, setModal] = useState(false);
  const [user, setUser] = useState(null);
  const [loader, setLoader] = useState(true);
  const [posts, setPosts] = useState([]);

  const opanModal = () => {
    setModal(true);
  };

  const closeModal = () => {
    setModal(false);
  };

  const register = async (user) => {
    const { username, email, password } = user;
    try {
      const res = await auth.createUserWithEmailAndPassword(email, password);
      res.user.updateProfile({ displayName: username });
      setModal(false);
    } catch (error) {
      console.log(error);
    }
  };

  const login = async (user) => {
    const { email, password } = user;
    const res = await auth.signInWithEmailAndPassword(email, password);
    setModal(false);
  };

  const logout = () => {
    auth
      .signOut()
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const create = (data) => {
    const { title, image } = data;
    const upload = storage.ref(`images/${image.name}`).put(image);
    upload.on(
      "state_changed",
      (snp) => {
        let progress = (snp.bytesTransferred / snp.totalBytes) * 100;
      },
      (err) => {
        console.log(err);
      },
      () => {
        storage
          .ref("images")
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            //success function/complete function
            db.collection("posts").add({
              title,
              image: url,
              username: user.displayName,
              currentTime: firebase.firestore.FieldValue.serverTimestamp(),
            });
          });
      }
    );
  };

  const publishComment = data => {
    const { id, comment } = data;
    db.collection("posts").doc(id).collection("comments").add({
        comment,
        username: user.displayName,
        currentTime: firebase.firestore.FieldValue.serverTimestamp(),
    });
  }

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      setUser(user);
      setLoader(false);
    });

    // fetch posts from firebase
    db.collection("posts")
      .orderBy("currentTime", "desc")
      .onSnapshot((snp) => {
        console.log("my data snp:: ", snp.docs);
        setPosts(
          snp.docs.map((doc) => ({
            id: doc.id,
            title: doc.data().title,
            image: doc.data().image,
            username: doc.data().username,
          }))
        );
      });
  }, [user, loader]);

  return (
    <ContextProvider.Provider
      value={{
        modal,
        opanModal,
        closeModal,
        register,
        login,
        logout,
        user,
        loader,
        create,
        posts,
        publishComment
      }}
    >
      {props.children}
    </ContextProvider.Provider>
  );
};

export default Context;
